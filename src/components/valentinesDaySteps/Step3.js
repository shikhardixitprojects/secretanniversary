import React from "react"

const Step3 = () => {
  return (
    <div className="align-center">
      <div
        style={{ fontSize: "18px", textAlign: "center", fontWeight: "bold" }}
      >
        Happy Valentine's Day, my love, my everything!!
      </div>
      <img
        style={{
          width: "auto",
          height: "800px",
          borderRadius: "10px",
        }}
        src={require("../../assets/valentines/Card3.jpg").default}
        title={"valentinesCard"}
        alt={"valentinesCardAlt"}
      />
    </div>
  )
}

export default Step3
