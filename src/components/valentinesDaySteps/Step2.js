import React from "react"

const Step2 = () => {
  return (
    <div className="align-center">
      <div
        style={{ fontSize: "18px", textAlign: "center", fontWeight: "bold" }}
      >
        Life always has it's ups and downs, but you're my constant.
      </div>
      <img
        style={{
          width: "auto",
          height: "800px",
          borderRadius: "10px",
        }}
        src={require("../../assets/valentines/Card2.jpg").default}
        title={"valentinesCard"}
        alt={"valentinesCardAlt"}
      />
    </div>
  )
}

export default Step2
